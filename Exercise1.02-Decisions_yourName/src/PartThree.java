import java.util.Scanner;
/*
 *  @Description: A number relation game
 *  @Author: Siddharth Natamai
 *  @Since September 14, 2017
 *  
 */
public class PartThree {

	public static void main(String[] args) {
		//Set up a Scanner object to get user input
		Scanner userInputOne = new Scanner(System.in);
		System.out.println("Enter your first number: ");
		int inputOne = userInputOne.nextInt();
		//Get another input from the Scanner object
		Scanner userInputTwo = new Scanner(System.in);
		System.out.println("Enter your second number: ");
		int inputTwo = userInputTwo.nextInt();
		//Get another input from the Scanner object
		Scanner userInputThree = new Scanner(System.in);
		System.out.println("Enter your third number: ");
		int inputThree = userInputThree.nextInt();
		
		//If-else statements to check if there is a relation between the inputs
		
		//Addition
		if (inputOne + inputTwo == inputThree)
			System.out.println("Congratulations " + inputOne + "+" + inputTwo + "=" + inputThree + ", you won!" );
		else
		
		//Subtraction	
		if(inputOne - inputTwo == inputThree)
			System.out.println("Congratulations " + inputOne + "-" + inputTwo + "=" + inputThree + ", you won!" );
		else
				
				if(inputTwo - inputOne == inputThree)
					System.out.println("Congratulations " + inputTwo + "-" + inputOne + "=" + inputThree + ", you won!" );
				else
		
		//Multiplication			
		if(inputOne * inputTwo == inputThree)
			System.out.println("Congratulations " + inputOne + "*" + inputTwo + "=" + inputThree + ", you won!" );
		else
			
				if(inputTwo * inputOne == inputThree)
					System.out.println("Congratulations " + inputTwo + "*" + inputOne + "=" + inputThree + ", you won!" );
				else
			
		//Division	
		if(inputOne / inputTwo == inputThree)
				System.out.println("Congratulations " + inputOne + "/" + inputTwo + "=" + inputThree + ", you won!" );
		else
			
			if(inputTwo / inputOne == inputThree)
				System.out.println("Congratulations " + inputTwo + "/" + inputOne + "=" + inputThree + ", you won!" );
			else
				System.out.println("Sorry, better luck next time");
		
				
				
				
		userInputOne.close();
		userInputTwo.close();
		userInputThree.close();
	}

}
