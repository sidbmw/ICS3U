import java.util.Scanner;
/*
 *  @Description: Simple program which displays the larger input number 
 *  @Author: Siddharth Natamai
 *  @Since September 13, 2017
 *  
 */
public class PartTwo {

	public static void main(String[] args) {
		//Set up a Scanner object to get user input
		Scanner userInputOne = new Scanner(System.in);
		System.out.println("Please enter a number: ");
		double inputOne = userInputOne.nextDouble();
		//Get another input from the Scanner object
		Scanner userInputTwo = new Scanner(System.in);
		System.out.println("Please enter a number: ");
		double inputTwo = userInputTwo.nextDouble();
		//If-else statement which displays a specific output in the console if inputOne is larger than inputTwo
		if(inputOne > inputTwo)
		{
            System.out.println(inputOne + " is greater than " + inputTwo);
		}
		
		else
		{
            System.out.println(inputTwo + " is greater than " + inputOne);

		}
		
		userInputOne.close();
		userInputTwo.close();

	}

}	