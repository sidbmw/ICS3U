import java.util.Scanner;
/*
 *  @Description: Checks input number to see if you are the lottery winner
 *  @Author: Siddharth Natamai
 *  @Since September 13, 2017
 *  
 */
public class PartOne {

	public static void main(String[] args) {
		
		//Set up a Scanner object to get user input
		Scanner userInput = new Scanner(System.in);
		System.out.println("Please enter a five digit number: ");
		
		String inputVal = userInput.next();
			
		//if-else statement to check if the input is five digits long
		if(inputVal.length() == 5 )
		{
				if(inputVal.equals("34567")) //if-else statement to check if input number is the winning number
					{
						System.out.println("You have won $1,000,000");
					}
				else
					{
						System.out.println("Better luck next time!");
					}
		}
		else
			{
				System.out.println(inputVal + " is not a valid 5 digit number");
			}
		
		
		userInput.close();
	}

}
