import java.util.Scanner;
/*
 *  @Description: Checks input number to see if you are the lottery winner
 *  @Author: Siddharth Natamai
 *  @Since September 14, 2017
 *  
 */
public class PartFour {

	public static void main(String[] args) {
		//Set up a Scanner object to get user input
		Scanner userInputOne = new Scanner(System.in);
		System.out.println("Enter the price of the DVD movie: ");
		double inputOne = userInputOne.nextDouble();
		//Get another input from the Scanner object
		Scanner userInputTwo = new Scanner(System.in);
		System.out.println("How many would you like to buy?: ");
		int inputTwo = userInputTwo.nextInt();
		
		
		//If-else statements to check for a valid input
		if(inputTwo >= 0) 
		{
			//Calculates subtotal, tax and total and prints it in the console
			final double subtotal, tax, total;
			double taxRate = 0.13;
			subtotal = inputOne * inputTwo;
			tax = subtotal * taxRate;
			total = subtotal + tax;
			System.out.println("\n\nSubtotal: $" + String.format("%.2f",subtotal));
			System.out.println("Tax: $" + String.format("%.2f",tax));
			System.out.println("Total Price: $" + String.format("%.2f",total));
		}
			else
				System.out.println("Total amount cannot be calculated if a negative number is entered");

		
		userInputOne.close();
		userInputTwo.close();		
	}

}
