package PartOne;

import java.util.Scanner;
/*
 *  @Description: Simple Program that forces the user to input a positive even number
 *  @Author: Siddharth Natamai
 *  @Since September 15, 2017
 *  
 */
public class PartOne {

	public static void main(String[] args) {
		//Scanner object which gets user input from the keyboard
		Scanner userInputOne = new Scanner(System.in);
		System.out.println("Enter a positive even number: ");
		int inputOne = userInputOne.nextInt();
		
		//if-else statement which makes sure the user input is greater than 0, is divisible by 2 and equal to 0(helps check if the input is a even number)
		if(inputOne > 0 && inputOne % 2 == 0)
		{
			System.out.println("You have entered a even number.");
			System.out.println("Half of " + inputOne + " is " + inputOne / 2);
		}
		else
			System.out.println("Your input is invalid, please enter a positive even number");
			Scanner userInputTwo = new Scanner(System.in);
			System.out.println("Enter a positive even number: ");	
			int inputTwo = userInputOne.nextInt();
			inputTwo = inputOne;
		
		
		
		userInputOne.close();

	}

}
