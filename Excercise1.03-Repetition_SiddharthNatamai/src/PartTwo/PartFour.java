package PartTwo;

/*
 *  @Description: Program that outputs a triangle of interesting numbers using repetition
 *  @Author: Siddharth Natamai
 *  @Since September 15, 2017
 *  
 */

public class PartFour {

	public static void main(String[] args) {
		
		//variable is declared and initialized
		int n123=1;
		
		//for loop which repeats until the value of i is less than or equal to 10. The value of i increases by one each time.
		for (int i=2; i<=10; i++) 
		{
		  for(int j=10;j>i;j--)System.out.print(" "); //for loop which repeats until the value of j is greater than i. The value of j is lowered each time.
		  System.out.println( n123 + " x 9 + "+i+" = "+(n123*9+i));
		  n123*=10;
		  n123+=i;
		}
	}

}
