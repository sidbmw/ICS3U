package PartTwo;

/*
 *  @Description: Program that outputs a triangle of interesting numbers using repetition
 *  @Author: Siddharth Natamai
 *  @Since September 15, 2017
 *  
 */

public class PartTwo {

	public static void main(String[] args) {
		
		//variable is declared and initialized
		long n123=1;
		
		//for loop which repeats the below code until the value of i is equal to 0 and less than 7. The value of i is increased by one each time.
		for (int i=0; i<7; i++) {
		  
			for(int j=10;j>i;j--)System.out.print("  "); //for loop which repeats until the value of j is greater than i. The value of j is lowered by one each time.
			System.out.println( n123 + " x " +n123 + " = "+(n123*n123));
			n123*=10;
			n123++;
		}

	}

}
