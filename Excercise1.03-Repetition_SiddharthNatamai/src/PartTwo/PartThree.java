package PartTwo;

/*
 *  @Description: Program that outputs a triangle of interesting numbers using repetition
 *  @Author: Siddharth Natamai
 *  @Since September 15, 2017
 *  
 */

public class PartThree {

	public static void main(String[] args) {
		
		//variable is declared and initialized
		int n123=9;
		
		//for loop which repeats until the value of i is greater than or equal to 0. The value of i is lowered by one each time.
		for (int i=7; i>=0; i--) 
		{
		  for(int j=-3;j<i;j++)System.out.print(" ");
		  System.out.println( n123 + " x 9 + "+i+" = "+(n123*9+i));
		  n123*=10;
		  n123+=i+1;
		}
	}

}
