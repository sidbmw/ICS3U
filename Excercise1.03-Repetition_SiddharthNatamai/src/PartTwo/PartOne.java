package PartTwo;

/*
 *  @Description: Program that outputs a triangle of interesting numbers using repetition
 *  @Author: Siddharth Natamai
 *  @Since September 15, 2017
 *  
 */

public class PartOne {

	public static void main(String[] args) {
		
		//2 variables are declared and initialized
		int n123=1;
		int n987=9;
		
		//for loop which repeats the below code until the value of i is less than or equal to 9. Value of i increases by one each time.
		for (int i=1; i<=9; i++)
		{
		  for(int j=9;j>i;j--)System.out.print(" ");//For loop which repeats until the value of j is equal to 9, j is greater than i and decreases the value of j by one each time
		  System.out.println( n123 + " x 8 + "+i+" = "+n987);
		  n123*=10;
		  n987*=10;
		  n123+=i+1;
		  n987+=9-i;
		
		
		
		}

	}

}
