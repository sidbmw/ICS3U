/*
 * Description: This program will work through various arithmetic operations and display to the console
 * Author: Siddharth Natamai
 * Date: September 7th 2017
 */
public class MathInJava {

	public static void main(String[] args) {
		//basic arithmetic operator are" +,-,*,/ and %
		
		//declare variables
		
		int num1, num2, num3;
		double dec1, dec2, dec3;
	
		//initialize some variables and investigate addition
		num1=5;
		num2=7;
		dec1=2.5;
		dec2=10;	//Note: println(dec2) would print put 10.0

		num3 = num1 + num2;
		dec3 = dec1 + dec2;
		System.out.println(num3 + " " + dec3);
		
		//This doesn't work because we are losing precision
		//num3=num1+dec1;
		
		dec1 = num1 + dec1;
		System.out.println(dec3);	//Works but still not great form
								 	//Should cast num1 as a double
		
		num3 = num1 + (int)dec1;	//cast dec1 as a int
		System.out.println(num3);	
		
		//I can add numbers to a variable
		num1 = num1 + 6;
		num2+=6;
		System.out.println("num1 is now" + num1 + "and num2 is now" + num2);
		
		//Increment by 1
		num1++;
		System.out.println("num1 incremented by one is" + num1);
		
		//Brackets and "" quotes matter
		// + for strings means concatenate
		// + for math means ADDITION
		
		System.out.println(15+5);
		System.out.println(15 + 5 + " = 15 + 5");
		System.out.println("15 + 5 = " + 15 + 5);
		System.out.println("15 + 5 = " + (15 + 5));
		
		//Division and Modulus
		//% show remainder after DIVISION (Modulus)
		System.out.println(98%10);	// 98/10 = 9 remainder 8
									// also note that if integer are the input then we get integers as output 
		
		//2 ways to have java use a double to show the decimals of int division
		System.out.println((double)num1/num2);
		System.out.println(1.0*num1/num2);
		
		//ASCII table shows the numeric value of a character (e.g. 'a' is 97)
		//We can change this numeric value by adding integers to it and it will change the character that is printed
		
		char letter = 'a'; //char needs SINGLE quotes when initializing
		letter++;
		System.out.println(letter); //b
		//Using the ASCII number
		char letter2=97;
		System.out.println(letter2); //the letter is 'a'
		

	}

}
