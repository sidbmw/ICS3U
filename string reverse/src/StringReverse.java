import java.util.*;
 
class StringReverse {

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);

        String original, original2, reverse = "", reverse2 = "";

        System.out.println("Enter string 1 to reverse");
        original = in.nextLine();
        System.out.println("Enter string 2 to reverse.");
        original2 = in.nextLine();

        int length = original.length();

        for (int i = length - 1; i >= 0; i--) {
            reverse = reverse + original.charAt(i);
        }

        int length2 = original2.length();

        for (int i = length2 - 1; i >= 0; i--) {
            reverse2 = reverse2 + original2.charAt(i);
        }

        System.out.println("Reverse of 1st entered string is: " + reverse);
        System.out.println("Reverse of 2nd entered string is: " + reverse2);
    }
}