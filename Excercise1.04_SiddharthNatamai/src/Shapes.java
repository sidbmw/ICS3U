import java.util.Scanner;
import java.util.*;
/*
 *  @Description: Simple Program that forces the user to input a positive even number
 *  @Author: Siddharth Natamai
 *  @Since September 25, 2017
 *  
 */

public class Shapes {

	public static void main(String[] args) {

		// Do-while loop and scanner object to get user's inputs
		do {
			Scanner userInput = new Scanner(System.in);
			System.out.print("You can view any of the following shapes: ");
			System.out.print("\n1 Square");
			System.out.print("\n2 Right-angle Triangle");
			System.out.print("\n3 Pyramid");
			System.out.print("\n4 Hourglass");
			System.out.print("\n5 Diamond");

			// Do-while loop that repeats until int from 1-5 is entered
			int shape;

			do {
				System.out.print("\nEnter a integer to choose a shape: ");
				try { // try catch to set shape to userInput
					shape = Integer.parseInt(userInput.nextLine());
				} catch (NumberFormatException e) {
					shape = -1;
				}
			} while (shape < 1 || shape > 5);

			// Do-while loop and try catch to set user input to inputOne after making sure
			// the input is valid
			int inputOne = -1;
			do {
				try {
					String str;
					do {
						System.out.print("\nEnter the height of the shape: ");
						str = userInput.nextLine();
					} while (!Character.isDigit(str.charAt(0)));
					inputOne = Integer.parseInt(str);
				} catch (Exception e) {
					inputOne = -1;
				}
			} while (inputOne < 1 || inputOne > 200);

			// Do-while loop to get a valid user input
			char ch;
			do {
				System.out.print("Enter a character: ");
				ch = userInput.nextLine().charAt(0);
			} while (ch == ' ');

			// if-else statements to check what the int shape value is
			if (shape == 1) {
				square(ch, inputOne);
				System.out.println();
			}

			if (shape == 2) {
				triangle(ch, inputOne);
				System.out.println();
			}

			if (shape == 3) {
				pyramid(ch, inputOne);
				System.out.println();
			}

			if (shape == 4) {
				hourglass(ch, inputOne);
				System.out.println();
			}

			if (shape == 5) {
				diamond(ch, inputOne);
				System.out.println();
			}
			
		} while (playAgain());

	}
	

	// method for play again feature
	private static boolean playAgain() {
		Scanner keyboard = new Scanner(System.in);
		System.out.print("Play again? (Y/N): ");
		String replay = keyboard.nextLine();
		System.out.println("\n");
		return replay.equalsIgnoreCase("Y");

	}

	// method for printing a square
	public static void square(char c, int n) {

		char[] a = new char[n];
		Arrays.fill(a, c);
		for (int i = n; i > 0; i--)

			System.out.println(a);

	}

	// method for printing a triangle
	public static void triangle(char c, int n) {

		for (int i = 1; i <= n; i++) {
			char[] a = new char[i];
			Arrays.fill(a, c);
			System.out.println(a);
		}

	}

	// method for printing a pyramid
	public static void pyramid(char c, int n) {

		for (int i = 0; i < n; i++) {
			char[] s = new char[n - i - 1];
			Arrays.fill(s, ' ');
			System.out.print(s);

			char[] a = new char[i * 2 + 1];
			Arrays.fill(a, c);
			System.out.println(a);
		}
	}

	// method for printing a diamond
	public static void diamond(char c, int n) {
		boolean odd = n % 2 == 1;
		n++;
		int mid = n / 2;
		int mi = mid;
		if (odd)
			mi--;
		for (int y = 1; y < n; y++) {
			for (int x = 1; x < n; x++) {
				System.out.print((Math.abs(x + y - n) > mi || Math.abs(x - y) > mi) ? ' ' : c);
			}
			System.out.println();
		}

	}

	// method for printing a hourglass
	public static void hourglass(char c, int n) {
		boolean odd = n % 2 == 1;
		if (odd)
			n++;
		int mid = n / 2;
		for (int y = 0; y < n; y++) {
			if (odd && y == mid)
				continue;
			for (int x = 1; x < n; x++) {
				int a = 0;
				if (Math.abs(x + y - mid) >= mid)
					a++;
				if (Math.abs(x - y - mid) >= mid)
					a++;
				System.out.print((a % 2 == 0) ? c : ' ');
			}
			System.out.println();
		}

	}

}