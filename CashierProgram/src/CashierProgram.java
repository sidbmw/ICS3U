import java.util.Scanner;
import java.text.DecimalFormat;

/*
 *  @Description: A simple cashier program
 *  @Author: Siddharth Natamai
 *  @Since September 8, 2017
 */


public class CashierProgram 
{

	public static void main(String[] args) 
	{
		//Set up a scanner object and get five user inputs from the keyboard
		Scanner itemOne = new Scanner(System.in);
		System.out.println("Please enter the price for item 1: ");
		double decimal_num1 =  itemOne.nextDouble();
		
		
		Scanner itemTwo = new Scanner(System.in);
		System.out.println("Please enter the price for item 2: ");
		double decimal_num2 =  itemTwo.nextDouble();

		
		Scanner itemThree = new Scanner(System.in);
		System.out.println("Please enter the price for item 3: ");
		double decimal_num3 =  itemThree.nextDouble();
		
		Scanner itemFour = new Scanner(System.in);
		System.out.println("Please enter the price for item 4: ");
		double decimal_num4 =  itemFour.nextDouble();
		
		Scanner itemFive = new Scanner(System.in);
		System.out.println("Please enter the price for item 5: ");
		double decimal_num5 =  itemFive.nextDouble();
		
		
		//The users input is printed in the console after rounding to 2 digits after the decimal
		DecimalFormat moneyFormat1 = new DecimalFormat("###.00");
		System.out.println("Item 1: $" + moneyFormat1.format(decimal_num1));
		
		DecimalFormat moneyFormat2 = new DecimalFormat("###.00");
		System.out.println("Item 2: $" + moneyFormat2.format(decimal_num2));
		
		DecimalFormat moneyFormat3 = new DecimalFormat("###.00");
		System.out.println("Item 3: $" + moneyFormat3.format(decimal_num3));
		
		DecimalFormat moneyFormat4 = new DecimalFormat("###.00");
		System.out.println("Item 4: $" + moneyFormat4.format(decimal_num4));
		
		DecimalFormat moneyFormat5 = new DecimalFormat("###.00");
		System.out.println("Item 5: $" + moneyFormat5.format(decimal_num5));
		
		//Subtotal, tax and total variables are created
		final double subtotal, tax, total;
		double taxRate = 0.13; //tax value of 13% is set to taxRate
		
		//Final values are calculated and printed in the console
		subtotal = decimal_num1 + decimal_num2 + decimal_num3 + decimal_num4 + decimal_num5;
		tax = subtotal * taxRate;
		total = subtotal + tax;
		System.out.println("\n\nSubtotal: $" + String.format("%.2f",subtotal));
		System.out.println("Tax: $" + String.format("%.2f",tax));
		System.out.println("Total Price: $" + String.format("%.2f",total));

		itemOne.close();
		itemTwo.close();
		itemThree.close();
		itemFour.close();
		itemFive.close();

	}

}