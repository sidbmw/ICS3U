import java.util.Scanner;

/*
 *  @Description: A program that helps convert seconds(input) to hours, minutes and seconds(output)
 *  @Author: Siddharth Natamai
 *  @Since September 11, 2017
 *  
 */


public class MovieLength {
   public static void main(String[] args) {
	  
	  //Set up scanner object and get a user input from the keyboard
      Scanner timeInput = new Scanner(System.in);
      System.out.print("Enter the movie length in seconds: ");
      //Set up input, hours, minutes and seconds variable to store and convert to hours, minutes and seconds
      int input = timeInput.nextInt();
      int hours = input / 3600;
      int minutes = (input % 3600) / 60;
      int seconds = (input % 3600) % 60;
      //Print out the hours, minutes and seconds value in the console
      System.out.println("\nHours: " + hours);
      System.out.println("Minutes: " + minutes);
      System.out.println("Seconds: " + seconds);
      //Close scanner object
      timeInput.close();
   }
}