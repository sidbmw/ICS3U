import java.util.Scanner;
import java.text.DecimalFormat;  //Needed for 2nd print method below

/* @description: This program demonstrates the decimal scanner and 
 * also decimal number formatting with the print statement
 * @author: Siddharth Natamai
 * @mod: Sept 8th, 2017
 */

public class FloatFormatting2 {

	public static void main(String[] args) {
		
		//Set up a scanner object to get input from keyboard
		Scanner inputStuff = new Scanner(System.in);
		
        //get another number via user input.  Note the .nextFloat()/.nextDouble() methods.
		System.out.println("Enter a decimal: ");
		double decimal_num = inputStuff.nextDouble();	
		
		//Now let's print this numbers out to the screen in 3
		//different ways!
		
		// 1) The Legacy "C" printf method
		System.out.printf("Method 1 Converted to 2 decimals: %.2f\n", decimal_num);
		//Note: that printf does not automatically line feed so you need to use \n
		
		
		// 2) The DecimalFormat object method
		DecimalFormat moneyFormat = new DecimalFormat("###.00");
		System.out.println("Method 2 Converted to 2 decimals: " + moneyFormat.format(decimal_num));
		
		// 3) The String.format method
		System.out.println("Method 3 Converted to 2 decimals: " + String.format("%.2f", decimal_num));
	
	    //Notice: In all 3 cases our result was rounded to 2 decimal places
		//This is different than casting where the decimals simply get truncated.
		
		//Also notice that for methods 1) and 3) that "f" in the formatting string %.2f represents 
		//a floating point number
		//If you want to print out an integer you would use a "d"
		System.out.printf("Method 1 with a cast to integer is: %d\n", (int)decimal_num);
		System.out.println("Method 3 with a cast to integer is: " + String.format("%d",(int)decimal_num));
		//Also string variables can be added with an "s"
		String newString="Hello ICS3U!";
		System.out.printf("Method 1 with an inserted string gives: %s\n", newString);
		System.out.printf("Method 3 with an inserted string gives: " + String.format("%s",  newString));
	}

}