/*
 * *This application demonstrates how to display text on the console.
 * @author Siddharth Natamai
 * Since Wednesday Sept 6th, 2017
 */






public class SampleOutput {

	//the main method is Always the first thing to run in Java
	public static void main(String[] args) {
		//the print method displays the text on thr console
		//but it Never moves to a new line by itself
		System.out.print ("Hello World");
		System.out.print("Are we on the next line yet?");
		
		System.out.print ("\n");
		//println moves the cursor on the console to a new line After it displays the text
		System.out.println ("Hello World");
		System.out.println ("Are we on the next line yet?")
		
		//\n can be used in the text of any orint statement
		//to move to the next line on the console
		System.out.println("\n\n\n\n\nLots of Blank Space!!!);"
		
		//use \"  

	}

}
